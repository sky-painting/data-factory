package com.tianhua.datafactory.domain.bo.bean;


import com.tianhua.datafactory.domain.bo.AbstractClassBean;

/**
 * Description:
 *
 * 领域接口
 * date: 2021/6/28
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class InterfaceBean extends AbstractClassBean {

}
