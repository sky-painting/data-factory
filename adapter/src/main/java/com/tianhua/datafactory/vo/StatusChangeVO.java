package com.tianhua.datafactory.vo;

import lombok.Data;

/**
 * Description
 * date: 2022/9/6
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class StatusChangeVO {
    private Integer status;
    private String moduleName;

}
