package com.tianhua.datafactory.vo.datafactory;

import lombok.Data;
import lombok.ToString;

/**
 * description: DataFactoryVo <br>
 * date: 2020/12/2 23:48 <br>
 * author: coderman <br>
 * version: 1.0 <br>
 */
@Data
@ToString
public class DataFactoryResponseVo {

}
