package com.tianhua.datafactory.vo.amis;

import lombok.Data;

/**
 * Description
 * date: 2022/9/8
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class ComponentVO {
    private String type;
    private String name;
    private String label;
}
