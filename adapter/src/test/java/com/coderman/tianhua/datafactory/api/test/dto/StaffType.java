package com.coderman.tianhua.datafactory.api.test.dto;

import lombok.Data;

/**
 * description: StaffType 员工类型数据源
 * date: 2021/1/21 22:28 <br>
 * author: coderman
 * version: 1.0 <br>
 */
@Data
public class StaffType {
    private int code;
    private String name;

}
