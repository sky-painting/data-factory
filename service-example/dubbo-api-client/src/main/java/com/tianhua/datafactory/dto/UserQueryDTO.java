package com.tianhua.datafactory.dto;

/**
 * Description
 * date: 2022/9/13
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class UserQueryDTO {

    private String userName;

    private String phone;

}
