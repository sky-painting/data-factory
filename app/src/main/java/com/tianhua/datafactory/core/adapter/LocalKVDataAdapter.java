package com.tianhua.datafactory.core.adapter;

/**
 * description: EnumDataAdaptor <br>
 * date: 2020/12/6 22:52 <br>
 * author: coderman <br>
 * version: 1.0 <br>
 */
public interface LocalKVDataAdapter {

    void transform2KV();
}
