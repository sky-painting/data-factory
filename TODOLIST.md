### 2.0.0 交付内容
1. 支持dubbo服务数据源接入(down)
2. 支持springboot,cloud数据源接入
3. 优化数据生成速度目前本地测（100条10个字段的模型大概2-3秒）(down)
4. 增加可视化界面配置(down)
5. 支持自定义数据集加载(数据加载，数据属性绑定,大文件映射,excel加载,txt加载,json加载)(down)
6. 调整工程模块(down)
7. 管理api模型,领域模型,数据库模型,数据库表字段,实现生命周期全管理(down)
8. 支持复杂对象参数和模型，返回结果的构建,field dsl rule(数组,图,集合)(down)
9. 实现http接口,返回接口的mock数据(入参为接口签名)(down)
10. 丰富数据源(增加相对常见的数据构建函数)(down)
11. 支持复杂对象关联数据生成(down)

### 2.1.0 交付内容
1. 支持构建的数据返回sql,excel,json的数据文本
2. 基于消息的数据生成（将生成的数据直接通过restTemplate调用远程接口插入,dubbo远程调用），使用SPI+MQ+独立模块的方案
3. 大数据量异步任务的方式数据生成，使用xxl-job的方案
4. 开发dubbo接口，为测试平台提供调用接口
5. 数据统计(支持项目统计,Api统计,参数模型统计,表字段统计,数据构建统计)
6. 对接网关进行数据mock(与测试平台进行同步)
7. client模块独立部署(构建api调用数据工厂远程平台)
8. 实现dubbo接口,返回接口的mock数据(入参为接口签名)
9. 支持多个注册中心，进行服务数据调用(主要spring cloud技术栈)
10. 支持导出各个模型的plantuml文档
11. 释放kv常量数据配置的能力
12. 支持随机数据使用加密算法加密
13. 支持属性间的脚本计算解析
14. 支持jsonstr的构建特性
15. 支持属性是枚举数据映射(如xxxType的值指定好之后根据枚举映射关系找到xxxTypeDesc)

### 2.3.0 交付内容 data-connector
1. 支持生成SQL Insert语句同步到数据库(优先Mysql)
2. 支持生成数据同步到消息中间件(kafka,plusar,rocketmq,es)
3. 支持生成接口入参并调用对应服务(springboot,http,dubbo)



数据工厂的服务职责：
1.对数据源进行管理
2.快速根据接口模型构建仿真数据
3.管理api模型,领域模型,数据库模型,数据库表字段模型
4.将模型参数与api(包括对外接口,服务内部接口和方法)和数据源进行挂钩
5.统一管理枚举,常量,配置项列表
6.构建常见数据源(姓名,电话号码)

